package com.example.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 00:46
 */
@SpringBootApplication(scanBasePackages = {"com.example.product","com.example.order"})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
