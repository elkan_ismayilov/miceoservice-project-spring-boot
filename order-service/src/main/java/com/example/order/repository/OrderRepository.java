package com.example.order.repository;

import com.example.order.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 18:23
 */
public interface OrderRepository extends JpaRepository<Order, Long> {
}
