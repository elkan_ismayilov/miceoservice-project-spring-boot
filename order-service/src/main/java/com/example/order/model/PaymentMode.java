package com.example.order.model;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 20:30
 */

public enum PaymentMode {
    CREDIT_CARD,
    DEBIT_CARD,
    CASH,
    PAYPAL,
    APPLE_PAY
}
