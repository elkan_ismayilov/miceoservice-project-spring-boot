package com.example.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 20:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {
    private long productId;
    private long quantity;
    private long totalAmount;
    private PaymentMode paymentMode;
}
