package com.example.order.service;

import com.example.order.model.OrderRequest;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 18:24
 */
public interface OrderService {
    long placeOrder(OrderRequest orderRequest);
}
