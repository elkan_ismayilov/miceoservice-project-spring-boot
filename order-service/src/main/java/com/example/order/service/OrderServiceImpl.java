package com.example.order.service;

import com.example.order.entity.Order;
import com.example.order.model.OrderRequest;
import com.example.order.repository.OrderRepository;
import com.example.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;

/**
 * microservice-parent
 * Elkhan
 * 21.01.2024 18:24
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final ProductService productService;
    @Override
    public long placeOrder(OrderRequest orderRequest) {
        productService.reduceQuantity(orderRequest.getProductId(), orderRequest.getQuantity());
        log.info("===> Creating order with status CREATED");
        Order order = Order.builder()
                .productId(orderRequest.getProductId())
                .quantity(orderRequest.getQuantity())
                .amount(orderRequest.getTotalAmount())
                .orderDate(Instant.now())
                .orderStatus("CREATED")
                .build();
        Order savedOrder = orderRepository.save(order);
        log.info("===> Order placed successfully with id: {}", orderRequest);
        return savedOrder.getId();
    }
}
