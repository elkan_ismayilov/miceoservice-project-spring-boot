package com.example.product.model;

import lombok.Data;

/**
 * microservice-parent
 * Elkhan
 * 16.01.2024 15:21
 */
@Data
public class ProductRequest {
    private String name;
    private long price;
    private long quantity;
}
