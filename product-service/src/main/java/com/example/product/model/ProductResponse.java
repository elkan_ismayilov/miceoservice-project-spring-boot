package com.example.product.model;

import lombok.Builder;
import lombok.Data;

/**
 * microservice-parent
 * Elkhan
 * 17.01.2024 20:56
 */
@Data
@Builder
public class ProductResponse {
    private long productId;
    private String name;
    private long price;
    private long quantity;
}
