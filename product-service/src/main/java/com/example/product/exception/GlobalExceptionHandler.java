package com.example.product.exception;

import com.example.product.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * microservice-parent
 * Elkhan
 * 17.01.2024 21:11
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HandleProductNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleProductNotFoundException(HandleProductNotFoundException e) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode(e.getErrorCode())
                .errorMessage(e.getMessage())
                .build();
        return ResponseEntity.status(404).body(errorResponse);
    }
}
