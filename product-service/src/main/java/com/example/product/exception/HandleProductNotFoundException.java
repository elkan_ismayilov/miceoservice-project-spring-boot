package com.example.product.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * microservice-parent
 * Elkhan
 * 17.01.2024 21:07
 */
@Getter
@Setter
public class HandleProductNotFoundException extends RuntimeException {
    private String errorCode;
    public HandleProductNotFoundException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}
