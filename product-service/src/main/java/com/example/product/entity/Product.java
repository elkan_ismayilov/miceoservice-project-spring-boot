package com.example.product.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * microservice-parent
 * Elkhan
 * 16.01.2024 14:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product", schema = "product")
@Builder
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long productId;
    private String name;
    private long price;
    private long quantity;
}
