package com.example.product.service;

import com.example.product.entity.Product;
import com.example.product.exception.HandleProductNotFoundException;
import com.example.product.model.ProductRequest;
import com.example.product.model.ProductResponse;
import com.example.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * microservice-parent
 * Elkhan
 * 16.01.2024 14:24
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public long addProduct(ProductRequest productRequest) {
        log.info("====> Adding product to database");
        Product product = Product.builder()
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .quantity(productRequest.getQuantity())
                .build();
        productRepository.save(product);
        log.info("====> Product added to database");
        return product.getProductId();
    }

    @Override
    public ProductResponse getProductById(Long productId) {
        log.info("====> Getting product from database");
        Product product = productRepository.findById(productId).orElseThrow(() ->
                new HandleProductNotFoundException("Product not found", "PRODUCT_NOT_FOUND"));
        return ProductResponse.builder()
                .productId(product.getProductId())
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
    }

    @Override
    public void reduceQuantity(Long productId, Long quantity) {
        log.info("====> Reducing product {} quantity for product ID {}", quantity, productId);
        Product product = productRepository.findById(productId).orElseThrow(() ->
                new HandleProductNotFoundException("Product not found", "PRODUCT_NOT_FOUND"));
        if (product.getQuantity() < quantity) {
            throw new HandleProductNotFoundException("Product quantity is not enough", "PRODUCT_QUANTITY_NOT_ENOUGH");
        }
        product.setQuantity(product.getQuantity() - quantity);
        productRepository.save(product);
        log.info("====> Product quantity reduced");
    }
}
